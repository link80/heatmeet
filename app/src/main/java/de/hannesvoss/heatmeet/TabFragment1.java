package de.hannesvoss.heatmeet;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ExecutionException;

public class TabFragment1 extends Fragment {
    public static JSONArray dataArray;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getJSONStuff();

        return inflater.inflate(R.layout.fragment_tab_fragment1, container, false);
    }

    private void getJSONStuff() {
        //  Getting the JSON stuff..
        try {
            JSONGetter abc = new JSONGetter(getContext());
            abc.execute(new URL("http://lowcost-env.hbhd2ykqxg.eu-central-1.elasticbeanstalk.com/data"));

            String result = abc.get();
            dataArray = new JSONArray(result);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}