package de.hannesvoss.heatmeet;

import android.Manifest;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.ErrorDialogFragment;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ExecutionException;

public class TabFragment2 extends Fragment {
    private GoogleMap map;
    private GoogleApiClient mGoogleApiClient;
    private MapView myOpenMapView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tab_fragment2, container, false);

        myOpenMapView = (MapView) view.findViewById(R.id.map);
        myOpenMapView.onCreate(savedInstanceState);
        myOpenMapView.onResume();

        myOpenMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap map) {
                loadMap(map);
                //  Zoom in Berlin
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(52.5170365, 13.3888599), 9.5f));
                setMarkers();
            }
        });

        return view;
    }

    protected void loadMap(GoogleMap googleMap) {
        map = googleMap;
        if (map != null) {
            // Map is ready
            Toast.makeText(getContext(), "Map Fragment was loaded properly!", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getContext(), "Error - Map was null!!", Toast.LENGTH_SHORT).show();
        }
    }

    private void setMarkers() {
        if (TabFragment1.dataArray != null) {
            try {
                for (int i = 0; i < TabFragment1.dataArray.length(); i++) {
                    try {
                        JSONObject containerObject = TabFragment1.dataArray.getJSONObject(i);

                        // Pulling items from the array
                        String latitude = containerObject.getString("latitude");
                        String longitude = containerObject.getString("longtitude");

                        // Setting Markers on Map!
                        Marker marker = map.addMarker(
                                new MarkerOptions()
                                        .position(new LatLng(Double.parseDouble(longitude), Double.parseDouble(latitude))));
                        Toast.makeText(getContext(), "DONE!", Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(getContext(), "ERROR", Toast.LENGTH_SHORT).show();
        }
    }
}