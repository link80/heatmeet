package de.hannesvoss.heatmeet;

import android.Manifest;
import android.app.Dialog;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.ErrorDialogFragment;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.TileOverlay;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.maps.android.heatmaps.Gradient;
import com.google.maps.android.heatmaps.HeatmapTileProvider;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class HomeFragment extends Fragment {
    public static GoogleMap map;
    private GoogleApiClient mGoogleApiClient;
    private MapView myOpenMapView;
    public static JSONArray dataArray;
    private final int getDataInterval = 5000;   // Eine Sekunde Intervall für das Holen von JSON-Daten
    private Handler handler;
    private Runnable runnable;
    private HeatmapTileProvider mProvider;
    private TileOverlay mOverlay;
    private List<LatLng> positions;
    private int[] heatmapColors = {Color.rgb(100, 165, 255), Color.rgb(0, 106, 255)};
    private float[] heatmapStartPoints = {0.03f, 1f};

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getJSONStuff();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tab_fragment2, container, false);

        getJSONStuff();

        myOpenMapView = (MapView) view.findViewById(R.id.map);
        myOpenMapView.onCreate(savedInstanceState);
        myOpenMapView.onResume();

        myOpenMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap map) {
                loadMap(map);
                //  Zoom in Berlin
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(52.5170365, 13.3888599), 9.5f));

                // Eigene Berechtigungen werden geprüft (intern) -> Alles ok zur Anzeige des eigenen Standortes?
                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    map.setMyLocationEnabled(true);
                } else {
                    Toast.makeText(getContext(), getString(R.string.myLocationError), Toast.LENGTH_SHORT).show();
                }
                /*Location loc = map.getMyLocation();
                if (loc != null) {
                    LatLng latLang = new LatLng(loc.getLatitude(), loc.getLongitude());
                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLang, 9.5f));
                }*/

                addHeatMap();

                handler = new Handler();
                runnable = new Runnable(){
                    public void run() {
                        // Hole Daten immer wieder..
                        getJSONStuff();
                        //postOwnPosition();
                        changeHeatmapData();
                        //Toast.makeText(getContext(), "INTERVAL", Toast.LENGTH_SHORT).show();
                        handler.postDelayed(runnable, getDataInterval);
                    }
                };
                handler.post(runnable);
            }
        });

        return view;
    }

    protected void loadMap(GoogleMap googleMap) {
        map = googleMap;
        if (map != null) {
            // Map is ready
            //Toast.makeText(getContext(), "Map Fragment was loaded properly!", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getContext(), "Error - Map was null!!", Toast.LENGTH_SHORT).show();
        }
    }

    private void addHeatMap() {
        // Get the data: latitude/longitude positions of police stations.
        try {
            positions = getLatLngList();
        } catch (Exception e) {
            Toast.makeText(getContext(), "Problem reading list of locations.", Toast.LENGTH_LONG).show();
        }

        Gradient gradient = new Gradient(heatmapColors, heatmapStartPoints);

        // Create a heat map tile provider, passing it the latlngs of the police stations.
        mProvider = new HeatmapTileProvider.Builder()
                .data(positions)
                .gradient(gradient)
                .build();

        // Add a tile overlay to the map, using the heat map tile provider.
        mOverlay = map.addTileOverlay(new TileOverlayOptions().tileProvider(mProvider));
    }

    private void changeHeatmapData() {
        //mProvider.setData(positions);
        mProvider.setOpacity(0.7);
        mOverlay.clearTileCache();
    }

    private List<LatLng> getLatLngList() {
        if (dataArray != null) {
            List<LatLng> result = new ArrayList<LatLng>();

            // Set the JSON results and put it in a list
            try {
                for (int i = 0; i < dataArray.length(); i++) {
                    try {
                        JSONObject containerObject = dataArray.getJSONObject(i);

                        // Pulling items from the array
                        String latitude     = containerObject.getString("latitude");
                        String longitude    = containerObject.getString("longtitude");

                        result.add(new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude)));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        } else {
            Toast.makeText(getContext(), "ERROR", Toast.LENGTH_SHORT).show();
            return null;
        }
    }

    private void getJSONStuff() {
        //  Getting the JSON stuff..
        try {
            JSONGetter abc = new JSONGetter(getContext());
            abc.execute(new URL("http://lowcost-env.hbhd2ykqxg.eu-central-1.elasticbeanstalk.com/data"));

            String result = abc.get();
            dataArray = new JSONArray(result);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void postOwnPosition() {
        try {
            JSONPost abc = new JSONPost();
            abc.execute();
            Toast.makeText(getContext(), abc.get(), Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String POST(String tmp, double longitude, double latitude) {
        return "";
    }
}